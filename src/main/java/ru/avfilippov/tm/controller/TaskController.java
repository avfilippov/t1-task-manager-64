package ru.avfilippov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.avfilippov.tm.enumerated.Status;
import ru.avfilippov.tm.model.Project;
import ru.avfilippov.tm.model.Task;
import ru.avfilippov.tm.repository.ProjectRepository;
import ru.avfilippov.tm.repository.TaskRepository;

import java.util.Collection;

@Controller
public final class TaskController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new Task("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        final Task task = taskRepository.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Status[] getStatuses() {
        return Status.values();
    }

    private Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

}
