package ru.avfilippov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.avfilippov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project {

    @Setter
    @Getter
    private String id = UUID.randomUUID().toString();

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private String description;

    @Setter
    @Getter
    private Status status = Status.NOT_STARTED;

    @Setter
    @Getter
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Setter
    @Getter
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public Project() {
    }

    public Project(final String name) {
        this.name = name;
    }
}
