package ru.avfilippov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.avfilippov.tm")
public class ApplicationConfiguration {

}
